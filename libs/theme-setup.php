<?php
/**
 * Functions and definitions
 */

if (!function_exists('vetcare_child_theme_setup')) {
    function vetcare_child_theme_setup()
    {
        add_theme_support(
            'editor-color-palette',
            array(
                array(
                    'name' => esc_html__('Primary', 'vetcare'),
                    'slug' => 'primary',
                    'color' => get_field("primary_color", "option"),
                ),
                array(
                    'name' => esc_html__('Secondary', 'vetcare'),
                    'slug' => 'secondary',
                    'color' => get_field("secondary_color", "option"),
                ),
                array(
                    'name' => esc_html__('Tertiary', 'vetcare'),
                    'slug' => 'tertiary',
                    'color' => get_field("tertiary_color", "option"),
                ),
                array(
                    'name' => esc_html__('Dark', 'vetcare'),
                    'slug' => 'dark',
                    'color' => get_field("dark_color", "option"),
                ),
                array(
                    'name' => esc_html__('White', 'vetcare'),
                    'slug' => 'white',
                    'color' => get_field("white_color", "option"),
                ),

            )
        );
    }
}

add_action('after_setup_theme', 'vetcare_child_theme_setup');
