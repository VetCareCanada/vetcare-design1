const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const TerserPlugin = require('terser-webpack-plugin');

module.exports = (env, argv) => {
  function isDevelopment() {
    return argv.mode === 'development';
  }

  let config = {
    entry: {
      //editor: './src/editor.js',
      script: './src/script.js'
    },
    output: {
      filename: '[name].js',
      clean: true, // Clean the output directory before emit.
    },
    module: {
      rules: [
       {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                '@babel/preset-env',
                [
                  '@babel/preset-react',
                  {
                    "pragma": "wp.element.createElement",
                    "pragmaFrag": "wp.element.Fragment",
                    "development": isDevelopment()
                  }
                ]
              ]
            }
          }
        },
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            MiniCSSExtractPlugin.loader,
            'css-loader',
            'sass-loader'
          ]
        }
      ]
    },
    optimization: {
      minimizer: [
        new TerserPlugin(),
        new CssMinimizerPlugin()
      ]
    },
    plugins: [
      new MiniCSSExtractPlugin({
        chunkFilename: "[id].css",
        filename: (chunkData) => {
          return chunkData.chunk.name === 'script' ? 'style.css' : '[name].css'
        }
      }),
      new CssMinimizerPlugin()
    ],
    devtool: isDevelopment() ? 'inline-source-map' : 'source-map',

    externals: {
      jquery: "jQuery"
    }
  };
  return config;
}