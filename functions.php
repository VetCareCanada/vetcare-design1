<?php

$template_path = get_stylesheet_directory();
$include_files = [
    '/inc/template-tags.php', // Custom template tags for the theme.
    '/libs/theme-setup.php',
];

if ($include_files) {
    foreach ($include_files as $key => $value) {
        if (file_exists($template_path . $value)) {
            require_once($template_path . $value);
        }
    }
}

if (!function_exists("vetcare_scripts")) {

    function vetcare_scripts()
    {
        // enqueue style
        wp_enqueue_style(
            'vetcare-style',
            get_stylesheet_directory_uri() . '/dist/style.css',
            array(),
            wp_get_theme()->get('Version')
        );

        // enqueue script
        wp_enqueue_script(
            'bootstrap-script',
            'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js',
            wp_get_theme()->get('Version'),
            true
        );
        wp_enqueue_script(
            'vetcare-script',
            get_stylesheet_directory_uri() . '/dist/script.js',
            array('jquery', 'bootstrap-script'),
            wp_get_theme()->get('Version'),
            true
        );
    }
}

add_action('wp_enqueue_scripts', 'vetcare_scripts');

//enqueue theme style in editor
//echo get_stylesheet_directory_uri() . '/dist/style.css';
add_editor_style(get_stylesheet_directory_uri() . '/dist/style.css');

//enqueue editor style
//add_editor_style(get_stylesheet_directory_uri()."/assets/dist/css/editor.css");


/**
 * setup theme primary and secondary fonts for gutenberg editor
 * setup css variables for gutenberg editor
 */
add_action('enqueue_block_editor_assets', function () {
    wp_enqueue_style('vc-primary-font-editor', get_field('primary_font_url', 'option'), false);
    wp_enqueue_style('vc-secondary-font-editor', get_field('secondary_font_url', 'option'), false);

    get_theme_setup_style(true);
});

/**
 * setup theme primary and secondary fonts
 * @return void
 */
function vc_theme_fonts()
{
    wp_enqueue_style('vc-primary-font', get_field('primary_font_url', 'option'), false);
    wp_enqueue_style('vc-secondary-font', get_field('secondary_font_url', 'option'), false);
}

add_action('wp_enqueue_scripts', 'vc_theme_fonts');

/**
 * setup colors
 * @return void
 *
 */
function vc_setup_colors()
{
    get_theme_setup_style();
    //add google analytics tag
    echo get_field("google_analytics_tag", "option");
}

add_action('wp_head', 'vc_setup_colors');

function vc_setup_body_start()
{
    get_theme_setup_style();
    //add google analytics tag
    echo get_field("body_start_scripts", "option");
}
add_action('wp_body_open', 'vc_setup_body_start');

function vc_setup_footer()
{
    get_theme_setup_style();
    //add google analytics tag
    echo get_field("footer_scripts", "option");
}
add_action('wp_footer', 'vc_setup_footer');