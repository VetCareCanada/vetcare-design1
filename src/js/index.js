import "./blocks/_wp-block-vc-blocks-vctabs";
import "./blocks/_wp-block-cover";

(function ($) {
    $(document).scroll(function () {
        const $nav = $(".navbar.fixed-top");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
    });

    $(document).ready(function () {
        const announcementBar = getCookie('announcementBar');
        if (!announcementBar) {
            $(".announcement-bar-wrap").show();
        }

        $(".announcement-bar-wrap .fa-times").click(function () {
            setCookie('announcementBar', '1', '7');
        });

        function setCookie(cname, cvalue, exdays) {
            const d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            let expires = "expires="+ d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }
        function getCookie(cname) {
            let name = cname + "=";
            let decodedCookie = decodeURIComponent(document.cookie);
            let ca = decodedCookie.split(';');
            for(let i = 0; i <ca.length; i++) {
                let c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    });

})(jQuery);