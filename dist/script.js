/******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/js/blocks/_wp-block-cover.js":
/*!******************************************!*\
  !*** ./src/js/blocks/_wp-block-cover.js ***!
  \******************************************/
/***/ (function() {

(function ($) {
  $(document).ready(function () {
    if ($(".has-zoom-animation").length > 0) {
      $(".has-zoom-animation").addClass("vc-animated");
    }
  });
})(jQuery);

/***/ }),

/***/ "./src/js/blocks/_wp-block-vc-blocks-vctabs.js":
/*!*****************************************************!*\
  !*** ./src/js/blocks/_wp-block-vc-blocks-vctabs.js ***!
  \*****************************************************/
/***/ (function() {

(function ($) {
  //tabs
  $(".wp-block-vc-blocks-vctab").click(function () {
    var tabPane = $(this).attr("data-vc-target");
    $(".vc-tab-pane").removeClass("active").removeClass("show");
    $(tabPane).addClass("active").addClass("show");
    $(this).siblings().removeClass("active");
    $(this).addClass("active");
  }); //tabs dropdown

  if ($(document).width() < 992) {
    var tabs = {};
    var tabsEl = $(".vctabs");

    if (tabsEl) {
      var tabsWrapperEl = tabsEl.find(".vctabs-wrapper");
      var activeTab = "";
      var tabsOptions = "";

      if (tabsWrapperEl.length) {
        var pElements = $(tabsWrapperEl).children(".vctab");
        pElements.each(function () {
          var tabText = $(this).find("p").text();
          tabs[$(this).attr("data-vc-target")] = tabText;

          if (activeTab === "" && $(this).hasClass("active")) {
            activeTab = tabText;
          }
        });
        console.log(tabs);
        debugger;

        if (tabs) {
          $.each(tabs, function (k, v) {
            tabsOptions += "<li><a class=\"dropdown-item\" data-vc-target=\"".concat(k, "\" href=\"javascript:;\">").concat(v, "</a></li>");
          });
          tabsEl.after("<div class=\"dropdown mb-5\">\n  <button class=\"btn btn-secondary dropdown-toggle\" type=\"button\" id=\"dropdownMenuVctabs\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">\n".concat(activeTab, "\n  </button>\n  <ul class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuVctabs\" id=\"dropdownMenuVctabsMenu\">\n   ").concat(tabsOptions, "\n  </ul>\n</div>")); //remove deskto ptabs

          $(tabsEl).remove();
        } //bind click event


        $("#dropdownMenuVctabsMenu a").on("click", function () {
          var tabId = $(this).attr("data-vc-target");
          var tabTitle = $(this).text(); //hide all tab panes

          $(".vc-tab-pane").removeClass("active").removeClass("show"); //show clicked tab pane

          $(tabId).addClass("active").addClass("show"); //change dropdown text

          $("#dropdownMenuVctabs").html(tabTitle);
        });
      }
    }
  }
})(jQuery);

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _blocks_wp_block_vc_blocks_vctabs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./blocks/_wp-block-vc-blocks-vctabs */ "./src/js/blocks/_wp-block-vc-blocks-vctabs.js");
/* harmony import */ var _blocks_wp_block_vc_blocks_vctabs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_blocks_wp_block_vc_blocks_vctabs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _blocks_wp_block_cover__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./blocks/_wp-block-cover */ "./src/js/blocks/_wp-block-cover.js");
/* harmony import */ var _blocks_wp_block_cover__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_blocks_wp_block_cover__WEBPACK_IMPORTED_MODULE_1__);



(function ($) {
  $(document).scroll(function () {
    var $nav = $(".navbar.fixed-top");
    $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
  });
  $(document).ready(function () {
    var announcementBar = getCookie('announcementBar');

    if (!announcementBar) {
      $(".announcement-bar-wrap").show();
    }

    $(".announcement-bar-wrap .fa-times").click(function () {
      setCookie('announcementBar', '1', '7');
    });

    function setCookie(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
      var expires = "expires=" + d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');

      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }

        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }

      return "";
    }
  });
})(jQuery);

/***/ }),

/***/ "./src/scss/index.scss":
/*!*****************************!*\
  !*** ./src/scss/index.scss ***!
  \*****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
!function() {
"use strict";
/*!***********************!*\
  !*** ./src/script.js ***!
  \***********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _scss_index_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./scss/index.scss */ "./src/scss/index.scss");
/* harmony import */ var _js_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/index */ "./src/js/index.js");


$(function () {
  $(".searchBtnMobile").click(function () {
    $("#autocomplete-textbox").toggleClass("active");
    setTimeout(function () {
      onClick();
      $('#autocomplete-textbox').trigger('touchstart');
    });

    function onClick() {
      debugger; // create invisible dummy input to receive the focus first

      var fakeInput = document.createElement('input');
      fakeInput.setAttribute('type', 'text');
      fakeInput.style.position = 'absolute';
      fakeInput.style.opacity = 0;
      fakeInput.style.height = 0;
      fakeInput.style.fontSize = '16px'; // disable auto zoom
      // you may need to append to another element depending on the browser's auto
      // zoom/scroll behavior

      document.body.prepend(fakeInput); // focus so that subsequent async focus will work

      fakeInput.focus();
      setTimeout(function () {
        // now we can focus on the target input
        $('#autocomplete-textbox').focus(); // cleanup

        fakeInput.remove();
      }, 1000);
    }
  });
});
}();
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2NyaXB0LmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLENBQUMsVUFBVUEsQ0FBVixFQUFhO0VBQ1ZBLENBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlDLEtBQVosQ0FBa0IsWUFBWTtJQUMxQixJQUFJRixDQUFDLENBQUMscUJBQUQsQ0FBRCxDQUF5QkcsTUFBekIsR0FBa0MsQ0FBdEMsRUFBeUM7TUFDckNILENBQUMsQ0FBQyxxQkFBRCxDQUFELENBQXlCSSxRQUF6QixDQUFrQyxhQUFsQztJQUNIO0VBQ0osQ0FKRDtBQUtILENBTkQsRUFNR0MsTUFOSDs7Ozs7Ozs7OztBQ0FBLENBQUMsVUFBVUwsQ0FBVixFQUFhO0VBQ1Y7RUFDQUEsQ0FBQyxDQUFDLDJCQUFELENBQUQsQ0FBK0JNLEtBQS9CLENBQXFDLFlBQVk7SUFFN0MsSUFBTUMsT0FBTyxHQUFHUCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFRLElBQVIsQ0FBYSxnQkFBYixDQUFoQjtJQUNBUixDQUFDLENBQUMsY0FBRCxDQUFELENBQWtCUyxXQUFsQixDQUE4QixRQUE5QixFQUF3Q0EsV0FBeEMsQ0FBb0QsTUFBcEQ7SUFDQVQsQ0FBQyxDQUFDTyxPQUFELENBQUQsQ0FBV0gsUUFBWCxDQUFvQixRQUFwQixFQUE4QkEsUUFBOUIsQ0FBdUMsTUFBdkM7SUFFQUosQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRVSxRQUFSLEdBQW1CRCxXQUFuQixDQUErQixRQUEvQjtJQUNBVCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFJLFFBQVIsQ0FBaUIsUUFBakI7RUFFSCxDQVRELEVBRlUsQ0FhVjs7RUFDQSxJQUFJSixDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZVSxLQUFaLEtBQXNCLEdBQTFCLEVBQStCO0lBQzNCLElBQUlDLElBQUksR0FBRyxFQUFYO0lBQ0EsSUFBTUMsTUFBTSxHQUFHYixDQUFDLENBQUMsU0FBRCxDQUFoQjs7SUFDQSxJQUFJYSxNQUFKLEVBQVk7TUFDUixJQUFNQyxhQUFhLEdBQUdELE1BQU0sQ0FBQ0UsSUFBUCxDQUFZLGlCQUFaLENBQXRCO01BQ0EsSUFBSUMsU0FBUyxHQUFHLEVBQWhCO01BQ0EsSUFBSUMsV0FBVyxHQUFHLEVBQWxCOztNQUNBLElBQUlILGFBQWEsQ0FBQ1gsTUFBbEIsRUFBMEI7UUFDdEIsSUFBSWUsU0FBUyxHQUFHbEIsQ0FBQyxDQUFDYyxhQUFELENBQUQsQ0FBaUJLLFFBQWpCLENBQTBCLFFBQTFCLENBQWhCO1FBQ0FELFNBQVMsQ0FBQ0UsSUFBVixDQUFlLFlBQVk7VUFDdkIsSUFBSUMsT0FBTyxHQUFHckIsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRZSxJQUFSLENBQWEsR0FBYixFQUFrQk8sSUFBbEIsRUFBZDtVQUNBVixJQUFJLENBQUNaLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVEsSUFBUixDQUFhLGdCQUFiLENBQUQsQ0FBSixHQUF1Q2EsT0FBdkM7O1VBQ0EsSUFBSUwsU0FBUyxLQUFLLEVBQWQsSUFBb0JoQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVF1QixRQUFSLENBQWlCLFFBQWpCLENBQXhCLEVBQW9EO1lBQ2hEUCxTQUFTLEdBQUdLLE9BQVo7VUFDSDtRQUNKLENBTkQ7UUFPQUcsT0FBTyxDQUFDQyxHQUFSLENBQVliLElBQVo7UUFDQTs7UUFDQSxJQUFJQSxJQUFKLEVBQVU7VUFDTlosQ0FBQyxDQUFDb0IsSUFBRixDQUFPUixJQUFQLEVBQWEsVUFBVWMsQ0FBVixFQUFhQyxDQUFiLEVBQWdCO1lBQ3pCVixXQUFXLDhEQUFvRFMsQ0FBcEQsc0NBQThFQyxDQUE5RSxjQUFYO1VBQ0gsQ0FGRDtVQUlBZCxNQUFNLENBQUNlLEtBQVAsZ01BRWxCWixTQUZrQixvSUFLZkMsV0FMZSx3QkFMTSxDQWNOOztVQUNBakIsQ0FBQyxDQUFDYSxNQUFELENBQUQsQ0FBVWdCLE1BQVY7UUFDSCxDQTNCcUIsQ0E4QnRCOzs7UUFDQTdCLENBQUMsQ0FBQywyQkFBRCxDQUFELENBQStCOEIsRUFBL0IsQ0FBa0MsT0FBbEMsRUFBMkMsWUFBWTtVQUNuRCxJQUFJQyxLQUFLLEdBQUcvQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFRLElBQVIsQ0FBYSxnQkFBYixDQUFaO1VBQ0EsSUFBSXdCLFFBQVEsR0FBR2hDLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXNCLElBQVIsRUFBZixDQUZtRCxDQUduRDs7VUFDQXRCLENBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0JTLFdBQWxCLENBQThCLFFBQTlCLEVBQXdDQSxXQUF4QyxDQUFvRCxNQUFwRCxFQUptRCxDQUtuRDs7VUFDQVQsQ0FBQyxDQUFDK0IsS0FBRCxDQUFELENBQVMzQixRQUFULENBQWtCLFFBQWxCLEVBQTRCQSxRQUE1QixDQUFxQyxNQUFyQyxFQU5tRCxDQU9uRDs7VUFDQUosQ0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUJpQyxJQUF6QixDQUE4QkQsUUFBOUI7UUFDSCxDQVREO01BVUg7SUFDSjtFQUNKO0FBR0osQ0FuRUQsRUFtRUczQixNQW5FSDs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQ0E7O0FBRUEsQ0FBQyxVQUFVTCxDQUFWLEVBQWE7RUFDVkEsQ0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWWlDLE1BQVosQ0FBbUIsWUFBWTtJQUMzQixJQUFNQyxJQUFJLEdBQUduQyxDQUFDLENBQUMsbUJBQUQsQ0FBZDtJQUNBbUMsSUFBSSxDQUFDQyxXQUFMLENBQWlCLFVBQWpCLEVBQTZCcEMsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRcUMsU0FBUixLQUFzQkYsSUFBSSxDQUFDRyxNQUFMLEVBQW5EO0VBQ0gsQ0FIRDtFQUtBdEMsQ0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWUMsS0FBWixDQUFrQixZQUFZO0lBQzFCLElBQU1xQyxlQUFlLEdBQUdDLFNBQVMsQ0FBQyxpQkFBRCxDQUFqQzs7SUFDQSxJQUFJLENBQUNELGVBQUwsRUFBc0I7TUFDbEJ2QyxDQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QnlDLElBQTVCO0lBQ0g7O0lBRUR6QyxDQUFDLENBQUMsa0NBQUQsQ0FBRCxDQUFzQ00sS0FBdEMsQ0FBNEMsWUFBWTtNQUNwRG9DLFNBQVMsQ0FBQyxpQkFBRCxFQUFvQixHQUFwQixFQUF5QixHQUF6QixDQUFUO0lBQ0gsQ0FGRDs7SUFJQSxTQUFTQSxTQUFULENBQW1CQyxLQUFuQixFQUEwQkMsTUFBMUIsRUFBa0NDLE1BQWxDLEVBQTBDO01BQ3RDLElBQU1DLENBQUMsR0FBRyxJQUFJQyxJQUFKLEVBQVY7TUFDQUQsQ0FBQyxDQUFDRSxPQUFGLENBQVVGLENBQUMsQ0FBQ0csT0FBRixLQUFlSixNQUFNLEdBQUMsRUFBUCxHQUFVLEVBQVYsR0FBYSxFQUFiLEdBQWdCLElBQXpDO01BQ0EsSUFBSUssT0FBTyxHQUFHLGFBQVlKLENBQUMsQ0FBQ0ssV0FBRixFQUExQjtNQUNBbEQsUUFBUSxDQUFDbUQsTUFBVCxHQUFrQlQsS0FBSyxHQUFHLEdBQVIsR0FBY0MsTUFBZCxHQUF1QixHQUF2QixHQUE2Qk0sT0FBN0IsR0FBdUMsU0FBekQ7SUFDSDs7SUFDRCxTQUFTVixTQUFULENBQW1CRyxLQUFuQixFQUEwQjtNQUN0QixJQUFJVSxJQUFJLEdBQUdWLEtBQUssR0FBRyxHQUFuQjtNQUNBLElBQUlXLGFBQWEsR0FBR0Msa0JBQWtCLENBQUN0RCxRQUFRLENBQUNtRCxNQUFWLENBQXRDO01BQ0EsSUFBSUksRUFBRSxHQUFHRixhQUFhLENBQUNHLEtBQWQsQ0FBb0IsR0FBcEIsQ0FBVDs7TUFDQSxLQUFJLElBQUlDLENBQUMsR0FBRyxDQUFaLEVBQWVBLENBQUMsR0FBRUYsRUFBRSxDQUFDckQsTUFBckIsRUFBNkJ1RCxDQUFDLEVBQTlCLEVBQWtDO1FBQzlCLElBQUlDLENBQUMsR0FBR0gsRUFBRSxDQUFDRSxDQUFELENBQVY7O1FBQ0EsT0FBT0MsQ0FBQyxDQUFDQyxNQUFGLENBQVMsQ0FBVCxLQUFlLEdBQXRCLEVBQTJCO1VBQ3ZCRCxDQUFDLEdBQUdBLENBQUMsQ0FBQ0UsU0FBRixDQUFZLENBQVosQ0FBSjtRQUNIOztRQUNELElBQUlGLENBQUMsQ0FBQ0csT0FBRixDQUFVVCxJQUFWLEtBQW1CLENBQXZCLEVBQTBCO1VBQ3RCLE9BQU9NLENBQUMsQ0FBQ0UsU0FBRixDQUFZUixJQUFJLENBQUNsRCxNQUFqQixFQUF5QndELENBQUMsQ0FBQ3hELE1BQTNCLENBQVA7UUFDSDtNQUNKOztNQUNELE9BQU8sRUFBUDtJQUNIO0VBQ0osQ0EvQkQ7QUFpQ0gsQ0F2Q0QsRUF1Q0dFLE1BdkNIOzs7Ozs7Ozs7Ozs7QUNIQTs7Ozs7OztVQ0FBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7O1VBRUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7Ozs7O1dDdEJBO1dBQ0E7V0FDQTtXQUNBLGVBQWUsNEJBQTRCO1dBQzNDLGVBQWU7V0FDZixpQ0FBaUMsV0FBVztXQUM1QztXQUNBOzs7OztXQ1BBO1dBQ0E7V0FDQTtXQUNBO1dBQ0EseUNBQXlDLHdDQUF3QztXQUNqRjtXQUNBO1dBQ0E7Ozs7O1dDUEEsOENBQThDOzs7OztXQ0E5QztXQUNBO1dBQ0E7V0FDQSx1REFBdUQsaUJBQWlCO1dBQ3hFO1dBQ0EsZ0RBQWdELGFBQWE7V0FDN0Q7Ozs7Ozs7Ozs7Ozs7O0FDTkM7QUFDRDtBQUVDTCxDQUFDLENBQUMsWUFBWTtFQUNWQSxDQUFDLENBQUMsa0JBQUQsQ0FBRCxDQUFzQk0sS0FBdEIsQ0FBNEIsWUFBWTtJQUNwQ04sQ0FBQyxDQUFDLHVCQUFELENBQUQsQ0FBMkJvQyxXQUEzQixDQUF1QyxRQUF2QztJQUNBMkIsVUFBVSxDQUFDLFlBQVk7TUFFbkJDLE9BQU87TUFFUGhFLENBQUMsQ0FBQyx1QkFBRCxDQUFELENBQTJCaUUsT0FBM0IsQ0FBbUMsWUFBbkM7SUFDSCxDQUxTLENBQVY7O0lBUUEsU0FBU0QsT0FBVCxHQUFtQjtNQUNmLFNBRGUsQ0FHZjs7TUFDQSxJQUFNRSxTQUFTLEdBQUdqRSxRQUFRLENBQUNrRSxhQUFULENBQXVCLE9BQXZCLENBQWxCO01BQ0FELFNBQVMsQ0FBQ0UsWUFBVixDQUF1QixNQUF2QixFQUErQixNQUEvQjtNQUNBRixTQUFTLENBQUNHLEtBQVYsQ0FBZ0JDLFFBQWhCLEdBQTJCLFVBQTNCO01BQ0FKLFNBQVMsQ0FBQ0csS0FBVixDQUFnQkUsT0FBaEIsR0FBMEIsQ0FBMUI7TUFDQUwsU0FBUyxDQUFDRyxLQUFWLENBQWdCL0IsTUFBaEIsR0FBeUIsQ0FBekI7TUFDQTRCLFNBQVMsQ0FBQ0csS0FBVixDQUFnQkcsUUFBaEIsR0FBMkIsTUFBM0IsQ0FUZSxDQVNtQjtNQUVsQztNQUNBOztNQUNBdkUsUUFBUSxDQUFDd0UsSUFBVCxDQUFjQyxPQUFkLENBQXNCUixTQUF0QixFQWJlLENBZWY7O01BQ0FBLFNBQVMsQ0FBQ1MsS0FBVjtNQUVBWixVQUFVLENBQUMsWUFBTTtRQUViO1FBQ0EvRCxDQUFDLENBQUMsdUJBQUQsQ0FBRCxDQUEyQjJFLEtBQTNCLEdBSGEsQ0FLYjs7UUFDQVQsU0FBUyxDQUFDckMsTUFBVjtNQUVILENBUlMsRUFRUCxJQVJPLENBQVY7SUFVSDtFQUNKLENBdkNEO0FBeUNILENBMUNBLENBQUQsQyIsInNvdXJjZXMiOlsid2VicGFjazovL3ZjLWJsb2Nrcy8uL3NyYy9qcy9ibG9ja3MvX3dwLWJsb2NrLWNvdmVyLmpzIiwid2VicGFjazovL3ZjLWJsb2Nrcy8uL3NyYy9qcy9ibG9ja3MvX3dwLWJsb2NrLXZjLWJsb2Nrcy12Y3RhYnMuanMiLCJ3ZWJwYWNrOi8vdmMtYmxvY2tzLy4vc3JjL2pzL2luZGV4LmpzIiwid2VicGFjazovL3ZjLWJsb2Nrcy8uL3NyYy9zY3NzL2luZGV4LnNjc3M/MDM1OCIsIndlYnBhY2s6Ly92Yy1ibG9ja3Mvd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vdmMtYmxvY2tzL3dlYnBhY2svcnVudGltZS9jb21wYXQgZ2V0IGRlZmF1bHQgZXhwb3J0Iiwid2VicGFjazovL3ZjLWJsb2Nrcy93ZWJwYWNrL3J1bnRpbWUvZGVmaW5lIHByb3BlcnR5IGdldHRlcnMiLCJ3ZWJwYWNrOi8vdmMtYmxvY2tzL3dlYnBhY2svcnVudGltZS9oYXNPd25Qcm9wZXJ0eSBzaG9ydGhhbmQiLCJ3ZWJwYWNrOi8vdmMtYmxvY2tzL3dlYnBhY2svcnVudGltZS9tYWtlIG5hbWVzcGFjZSBvYmplY3QiLCJ3ZWJwYWNrOi8vdmMtYmxvY2tzLy4vc3JjL3NjcmlwdC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gKCQpIHtcclxuICAgICQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAoJChcIi5oYXMtem9vbS1hbmltYXRpb25cIikubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAkKFwiLmhhcy16b29tLWFuaW1hdGlvblwiKS5hZGRDbGFzcyhcInZjLWFuaW1hdGVkXCIpO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG59KShqUXVlcnkpOyIsIihmdW5jdGlvbiAoJCkge1xyXG4gICAgLy90YWJzXHJcbiAgICAkKFwiLndwLWJsb2NrLXZjLWJsb2Nrcy12Y3RhYlwiKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcblxyXG4gICAgICAgIGNvbnN0IHRhYlBhbmUgPSAkKHRoaXMpLmF0dHIoXCJkYXRhLXZjLXRhcmdldFwiKTtcclxuICAgICAgICAkKFwiLnZjLXRhYi1wYW5lXCIpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpLnJlbW92ZUNsYXNzKFwic2hvd1wiKTtcclxuICAgICAgICAkKHRhYlBhbmUpLmFkZENsYXNzKFwiYWN0aXZlXCIpLmFkZENsYXNzKFwic2hvd1wiKTtcclxuXHJcbiAgICAgICAgJCh0aGlzKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xyXG4gICAgICAgICQodGhpcykuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XHJcblxyXG4gICAgfSk7XHJcblxyXG4gICAgLy90YWJzIGRyb3Bkb3duXHJcbiAgICBpZiAoJChkb2N1bWVudCkud2lkdGgoKSA8IDk5Mikge1xyXG4gICAgICAgIGxldCB0YWJzID0ge307XHJcbiAgICAgICAgY29uc3QgdGFic0VsID0gJChcIi52Y3RhYnNcIik7XHJcbiAgICAgICAgaWYgKHRhYnNFbCkge1xyXG4gICAgICAgICAgICBjb25zdCB0YWJzV3JhcHBlckVsID0gdGFic0VsLmZpbmQoXCIudmN0YWJzLXdyYXBwZXJcIik7XHJcbiAgICAgICAgICAgIGxldCBhY3RpdmVUYWIgPSBcIlwiO1xyXG4gICAgICAgICAgICBsZXQgdGFic09wdGlvbnMgPSBcIlwiO1xyXG4gICAgICAgICAgICBpZiAodGFic1dyYXBwZXJFbC5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIGxldCBwRWxlbWVudHMgPSAkKHRhYnNXcmFwcGVyRWwpLmNoaWxkcmVuKFwiLnZjdGFiXCIpO1xyXG4gICAgICAgICAgICAgICAgcEVsZW1lbnRzLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCB0YWJUZXh0ID0gJCh0aGlzKS5maW5kKFwicFwiKS50ZXh0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGFic1skKHRoaXMpLmF0dHIoXCJkYXRhLXZjLXRhcmdldFwiKV0gPSB0YWJUZXh0O1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChhY3RpdmVUYWIgPT09IFwiXCIgJiYgJCh0aGlzKS5oYXNDbGFzcyhcImFjdGl2ZVwiKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY3RpdmVUYWIgPSB0YWJUZXh0O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2codGFicyk7XHJcbiAgICAgICAgICAgICAgICBkZWJ1Z2dlclxyXG4gICAgICAgICAgICAgICAgaWYgKHRhYnMpIHtcclxuICAgICAgICAgICAgICAgICAgICAkLmVhY2godGFicywgZnVuY3Rpb24gKGssIHYpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGFic09wdGlvbnMgKz0gYDxsaT48YSBjbGFzcz1cImRyb3Bkb3duLWl0ZW1cIiBkYXRhLXZjLXRhcmdldD1cIiR7a31cIiBocmVmPVwiamF2YXNjcmlwdDo7XCI+JHt2fTwvYT48L2xpPmA7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRhYnNFbC5hZnRlcihgPGRpdiBjbGFzcz1cImRyb3Bkb3duIG1iLTVcIj5cclxuICA8YnV0dG9uIGNsYXNzPVwiYnRuIGJ0bi1zZWNvbmRhcnkgZHJvcGRvd24tdG9nZ2xlXCIgdHlwZT1cImJ1dHRvblwiIGlkPVwiZHJvcGRvd25NZW51VmN0YWJzXCIgZGF0YS1icy10b2dnbGU9XCJkcm9wZG93blwiIGFyaWEtZXhwYW5kZWQ9XCJmYWxzZVwiPlxyXG4ke2FjdGl2ZVRhYn1cclxuICA8L2J1dHRvbj5cclxuICA8dWwgY2xhc3M9XCJkcm9wZG93bi1tZW51XCIgYXJpYS1sYWJlbGxlZGJ5PVwiZHJvcGRvd25NZW51VmN0YWJzXCIgaWQ9XCJkcm9wZG93bk1lbnVWY3RhYnNNZW51XCI+XHJcbiAgICR7dGFic09wdGlvbnN9XHJcbiAgPC91bD5cclxuPC9kaXY+YCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vcmVtb3ZlIGRlc2t0byBwdGFic1xyXG4gICAgICAgICAgICAgICAgICAgICQodGFic0VsKS5yZW1vdmUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgICAgICAgICAgLy9iaW5kIGNsaWNrIGV2ZW50XHJcbiAgICAgICAgICAgICAgICAkKFwiI2Ryb3Bkb3duTWVudVZjdGFic01lbnUgYVwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgdGFiSWQgPSAkKHRoaXMpLmF0dHIoXCJkYXRhLXZjLXRhcmdldFwiKTtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgdGFiVGl0bGUgPSAkKHRoaXMpLnRleHQoKTtcclxuICAgICAgICAgICAgICAgICAgICAvL2hpZGUgYWxsIHRhYiBwYW5lc1xyXG4gICAgICAgICAgICAgICAgICAgICQoXCIudmMtdGFiLXBhbmVcIikucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIikucmVtb3ZlQ2xhc3MoXCJzaG93XCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vc2hvdyBjbGlja2VkIHRhYiBwYW5lXHJcbiAgICAgICAgICAgICAgICAgICAgJCh0YWJJZCkuYWRkQ2xhc3MoXCJhY3RpdmVcIikuYWRkQ2xhc3MoXCJzaG93XCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vY2hhbmdlIGRyb3Bkb3duIHRleHRcclxuICAgICAgICAgICAgICAgICAgICAkKFwiI2Ryb3Bkb3duTWVudVZjdGFic1wiKS5odG1sKHRhYlRpdGxlKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuXHJcbn0pKGpRdWVyeSk7IiwiaW1wb3J0IFwiLi9ibG9ja3MvX3dwLWJsb2NrLXZjLWJsb2Nrcy12Y3RhYnNcIjtcclxuaW1wb3J0IFwiLi9ibG9ja3MvX3dwLWJsb2NrLWNvdmVyXCI7XHJcblxyXG4oZnVuY3Rpb24gKCQpIHtcclxuICAgICQoZG9jdW1lbnQpLnNjcm9sbChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgY29uc3QgJG5hdiA9ICQoXCIubmF2YmFyLmZpeGVkLXRvcFwiKTtcclxuICAgICAgICAkbmF2LnRvZ2dsZUNsYXNzKCdzY3JvbGxlZCcsICQodGhpcykuc2Nyb2xsVG9wKCkgPiAkbmF2LmhlaWdodCgpKTtcclxuICAgIH0pO1xyXG5cclxuICAgICQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBjb25zdCBhbm5vdW5jZW1lbnRCYXIgPSBnZXRDb29raWUoJ2Fubm91bmNlbWVudEJhcicpO1xyXG4gICAgICAgIGlmICghYW5ub3VuY2VtZW50QmFyKSB7XHJcbiAgICAgICAgICAgICQoXCIuYW5ub3VuY2VtZW50LWJhci13cmFwXCIpLnNob3coKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICQoXCIuYW5ub3VuY2VtZW50LWJhci13cmFwIC5mYS10aW1lc1wiKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHNldENvb2tpZSgnYW5ub3VuY2VtZW50QmFyJywgJzEnLCAnNycpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBmdW5jdGlvbiBzZXRDb29raWUoY25hbWUsIGN2YWx1ZSwgZXhkYXlzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGQgPSBuZXcgRGF0ZSgpO1xyXG4gICAgICAgICAgICBkLnNldFRpbWUoZC5nZXRUaW1lKCkgKyAoZXhkYXlzKjI0KjYwKjYwKjEwMDApKTtcclxuICAgICAgICAgICAgbGV0IGV4cGlyZXMgPSBcImV4cGlyZXM9XCIrIGQudG9VVENTdHJpbmcoKTtcclxuICAgICAgICAgICAgZG9jdW1lbnQuY29va2llID0gY25hbWUgKyBcIj1cIiArIGN2YWx1ZSArIFwiO1wiICsgZXhwaXJlcyArIFwiO3BhdGg9L1wiO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmdW5jdGlvbiBnZXRDb29raWUoY25hbWUpIHtcclxuICAgICAgICAgICAgbGV0IG5hbWUgPSBjbmFtZSArIFwiPVwiO1xyXG4gICAgICAgICAgICBsZXQgZGVjb2RlZENvb2tpZSA9IGRlY29kZVVSSUNvbXBvbmVudChkb2N1bWVudC5jb29raWUpO1xyXG4gICAgICAgICAgICBsZXQgY2EgPSBkZWNvZGVkQ29va2llLnNwbGl0KCc7Jyk7XHJcbiAgICAgICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPGNhLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgYyA9IGNhW2ldO1xyXG4gICAgICAgICAgICAgICAgd2hpbGUgKGMuY2hhckF0KDApID09ICcgJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGMgPSBjLnN1YnN0cmluZygxKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChjLmluZGV4T2YobmFtZSkgPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBjLnN1YnN0cmluZyhuYW1lLmxlbmd0aCwgYy5sZW5ndGgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBcIlwiO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxufSkoalF1ZXJ5KTsiLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW5cbmV4cG9ydCB7fTsiLCIvLyBUaGUgbW9kdWxlIGNhY2hlXG52YXIgX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fID0ge307XG5cbi8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG5mdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuXHR2YXIgY2FjaGVkTW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXTtcblx0aWYgKGNhY2hlZE1vZHVsZSAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0cmV0dXJuIGNhY2hlZE1vZHVsZS5leHBvcnRzO1xuXHR9XG5cdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG5cdHZhciBtb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdID0ge1xuXHRcdC8vIG5vIG1vZHVsZS5pZCBuZWVkZWRcblx0XHQvLyBubyBtb2R1bGUubG9hZGVkIG5lZWRlZFxuXHRcdGV4cG9ydHM6IHt9XG5cdH07XG5cblx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG5cdF9fd2VicGFja19tb2R1bGVzX19bbW9kdWxlSWRdKG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG5cdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG5cdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbn1cblxuIiwiLy8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbl9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuXHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cblx0XHRmdW5jdGlvbigpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcblx0XHRmdW5jdGlvbigpIHsgcmV0dXJuIG1vZHVsZTsgfTtcblx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgeyBhOiBnZXR0ZXIgfSk7XG5cdHJldHVybiBnZXR0ZXI7XG59OyIsIi8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb25zIGZvciBoYXJtb255IGV4cG9ydHNcbl9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIGRlZmluaXRpb24pIHtcblx0Zm9yKHZhciBrZXkgaW4gZGVmaW5pdGlvbikge1xuXHRcdGlmKF9fd2VicGFja19yZXF1aXJlX18ubyhkZWZpbml0aW9uLCBrZXkpICYmICFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywga2V5KSkge1xuXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIGtleSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGRlZmluaXRpb25ba2V5XSB9KTtcblx0XHR9XG5cdH1cbn07IiwiX193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqLCBwcm9wKSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBwcm9wKTsgfSIsIi8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbl9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcblx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG5cdH1cblx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbn07IiwiIGltcG9ydCAnLi9zY3NzL2luZGV4LnNjc3MnO1xyXG5pbXBvcnQgJy4vanMvaW5kZXgnO1xyXG5cclxuICQoZnVuY3Rpb24gKCkge1xyXG4gICAgICQoXCIuc2VhcmNoQnRuTW9iaWxlXCIpLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgJChcIiNhdXRvY29tcGxldGUtdGV4dGJveFwiKS50b2dnbGVDbGFzcyhcImFjdGl2ZVwiKTtcclxuICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcblxyXG4gICAgICAgICAgICAgb25DbGljaygpO1xyXG5cclxuICAgICAgICAgICAgICQoJyNhdXRvY29tcGxldGUtdGV4dGJveCcpLnRyaWdnZXIoJ3RvdWNoc3RhcnQnKTtcclxuICAgICAgICAgfSk7XHJcblxyXG5cclxuICAgICAgICAgZnVuY3Rpb24gb25DbGljaygpIHtcclxuICAgICAgICAgICAgIGRlYnVnZ2VyXHJcblxyXG4gICAgICAgICAgICAgLy8gY3JlYXRlIGludmlzaWJsZSBkdW1teSBpbnB1dCB0byByZWNlaXZlIHRoZSBmb2N1cyBmaXJzdFxyXG4gICAgICAgICAgICAgY29uc3QgZmFrZUlucHV0ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaW5wdXQnKVxyXG4gICAgICAgICAgICAgZmFrZUlucHV0LnNldEF0dHJpYnV0ZSgndHlwZScsICd0ZXh0JylcclxuICAgICAgICAgICAgIGZha2VJbnB1dC5zdHlsZS5wb3NpdGlvbiA9ICdhYnNvbHV0ZSdcclxuICAgICAgICAgICAgIGZha2VJbnB1dC5zdHlsZS5vcGFjaXR5ID0gMFxyXG4gICAgICAgICAgICAgZmFrZUlucHV0LnN0eWxlLmhlaWdodCA9IDBcclxuICAgICAgICAgICAgIGZha2VJbnB1dC5zdHlsZS5mb250U2l6ZSA9ICcxNnB4JyAvLyBkaXNhYmxlIGF1dG8gem9vbVxyXG5cclxuICAgICAgICAgICAgIC8vIHlvdSBtYXkgbmVlZCB0byBhcHBlbmQgdG8gYW5vdGhlciBlbGVtZW50IGRlcGVuZGluZyBvbiB0aGUgYnJvd3NlcidzIGF1dG9cclxuICAgICAgICAgICAgIC8vIHpvb20vc2Nyb2xsIGJlaGF2aW9yXHJcbiAgICAgICAgICAgICBkb2N1bWVudC5ib2R5LnByZXBlbmQoZmFrZUlucHV0KVxyXG5cclxuICAgICAgICAgICAgIC8vIGZvY3VzIHNvIHRoYXQgc3Vic2VxdWVudCBhc3luYyBmb2N1cyB3aWxsIHdvcmtcclxuICAgICAgICAgICAgIGZha2VJbnB1dC5mb2N1cygpXHJcblxyXG4gICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcblxyXG4gICAgICAgICAgICAgICAgIC8vIG5vdyB3ZSBjYW4gZm9jdXMgb24gdGhlIHRhcmdldCBpbnB1dFxyXG4gICAgICAgICAgICAgICAgICQoJyNhdXRvY29tcGxldGUtdGV4dGJveCcpLmZvY3VzKClcclxuXHJcbiAgICAgICAgICAgICAgICAgLy8gY2xlYW51cFxyXG4gICAgICAgICAgICAgICAgIGZha2VJbnB1dC5yZW1vdmUoKVxyXG5cclxuICAgICAgICAgICAgIH0sIDEwMDApXHJcblxyXG4gICAgICAgICB9XHJcbiAgICAgfSk7XHJcblxyXG4gfSkiXSwibmFtZXMiOlsiJCIsImRvY3VtZW50IiwicmVhZHkiLCJsZW5ndGgiLCJhZGRDbGFzcyIsImpRdWVyeSIsImNsaWNrIiwidGFiUGFuZSIsImF0dHIiLCJyZW1vdmVDbGFzcyIsInNpYmxpbmdzIiwid2lkdGgiLCJ0YWJzIiwidGFic0VsIiwidGFic1dyYXBwZXJFbCIsImZpbmQiLCJhY3RpdmVUYWIiLCJ0YWJzT3B0aW9ucyIsInBFbGVtZW50cyIsImNoaWxkcmVuIiwiZWFjaCIsInRhYlRleHQiLCJ0ZXh0IiwiaGFzQ2xhc3MiLCJjb25zb2xlIiwibG9nIiwiayIsInYiLCJhZnRlciIsInJlbW92ZSIsIm9uIiwidGFiSWQiLCJ0YWJUaXRsZSIsImh0bWwiLCJzY3JvbGwiLCIkbmF2IiwidG9nZ2xlQ2xhc3MiLCJzY3JvbGxUb3AiLCJoZWlnaHQiLCJhbm5vdW5jZW1lbnRCYXIiLCJnZXRDb29raWUiLCJzaG93Iiwic2V0Q29va2llIiwiY25hbWUiLCJjdmFsdWUiLCJleGRheXMiLCJkIiwiRGF0ZSIsInNldFRpbWUiLCJnZXRUaW1lIiwiZXhwaXJlcyIsInRvVVRDU3RyaW5nIiwiY29va2llIiwibmFtZSIsImRlY29kZWRDb29raWUiLCJkZWNvZGVVUklDb21wb25lbnQiLCJjYSIsInNwbGl0IiwiaSIsImMiLCJjaGFyQXQiLCJzdWJzdHJpbmciLCJpbmRleE9mIiwic2V0VGltZW91dCIsIm9uQ2xpY2siLCJ0cmlnZ2VyIiwiZmFrZUlucHV0IiwiY3JlYXRlRWxlbWVudCIsInNldEF0dHJpYnV0ZSIsInN0eWxlIiwicG9zaXRpb24iLCJvcGFjaXR5IiwiZm9udFNpemUiLCJib2R5IiwicHJlcGVuZCIsImZvY3VzIl0sInNvdXJjZVJvb3QiOiIifQ==